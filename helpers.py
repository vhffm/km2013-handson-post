"""
Helper Functions for Kevin-Helmholtz Demo in Ramses.

Volker Hoffmann <volker@cheleb.net>
22 May 2013
"""

import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt

def test_derivatives():
    """Test Derivative Computation and Back-Interpolation onto Grid."""

    # Define the function f(x,y) on mesh:
    nx = ny = 17
    x, y = np.meshgrid(np.linspace(0,1,nx), np.linspace(0,1,ny))
    f = np.ones([nx,ny]); f[(nx-1)/2,(ny-1)/2] = 10

    # Compute Derivatives, Back-Interpolate
    dfdx, dfdy = compute_derivatives(f, x, y)

    # Plots
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    im = ax.imshow(f, interpolation='none')
    plt.colorbar(im)
    ax.set_title('f(x,y)')

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    im = ax.imshow(dfdx, interpolation='none')
    plt.colorbar(im)
    ax.set_title('df(x,y)/dx')

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    im = ax.imshow(dfdx, interpolation='none')
    plt.colorbar(im)
    ax.set_title('df(x,y)/dy')

    plt.show()

def compute_derivatives(f, x, y):
    """
    Compute Numerical Derivatives df/dx and df/dy and re-interpolates their 
    values onto the original grid. The input grid is assumed to be cartesian
    and regular, i.e. dx=dy=const.

    @params = f, x, y - plaid matrices, i.e.
    x = [[ 1 1 1 ]
         [ 2 2 2 ]]
    y = [[ 1 1 1 ]
         [ 2 2 2 ]]
    f = [[ 4 5 6 ]
           7 8 9 ]]

    @returns = dfdx, dfdy - plaid matrices like f
    """

    # Determine Mesh Dimensions
    if (x.shape == f.shape) and (x.shape == y.shape):
        nx = x.shape[0]; ny = x.shape[1]
    else:
        raise Exception('Different Shapes for Input Matrices.')

    # Compute partial derivatives of f(x,y) along the two mesh directions
    dfdx = np.diff(f, axis=1) / (1./nx)
    dfdy = np.diff(f, axis=0) / (1./ny)
    
    # Location of derivatives on grid -- dfdx(xc,y), dfdy(x,yc)
    xc = (x[:,1:] + x[:,:-1])/2
    yc = (y[1:,:] + y[:-1,:])/2
    
    # Reshaping so we can interpolate
    xx = x[:-1,:]
    yy = y[:,:-1]
    px = (xc.reshape(xc.size), yy.reshape(yy.size))
    py = (xx.reshape(xx.size), yc.reshape(yc.size))

    # Interpolate back onto (x,y) grid
    dfdx2 = griddata(px, \
            dfdx.reshape(dfdx.size), (x, y), \
            method='linear')
    dfdy2 = griddata(py, \
            dfdy.reshape(dfdy.size), (x, y), \
            method='linear')

    # Return Results
    return dfdx2, dfdy2

def mkpoints(nx, ny):
    """
    Generate Sample Points. This is slighly painful because Pymses expects
    the arrays to have a particular shape.
    """

    print "** Generating Sampling Points..."

    # Method 1
    xx, yy = np.meshgrid(np.linspace(0, 1, nx), np.linspace(0, 1, ny))
    xx = xx.reshape(nx*ny); yy = yy.reshape(nx*ny)

    # Method 2
    # px = np.linspace(0, 1, nx); py = np.linspace(0, 1, ny)
    # xx = np.zeros(nx*ny); yy = np.zeros(nx*ny)
    # ix = 0; iy = 0
    # for ii in range(nx*ny):
    #     xx[ii] = px[ix]; yy[ii] = py[iy]
    #     if ix == nx-1: ix = 0; iy += 1
    #     else: ix += 1

    # Rearranging for Pymses
    pp = np.zeros((xx.shape[0], 2))
    for ii in range(xx.shape[0]):
        pp[ii,:] = np.array([xx[ii], yy[ii]])

    return pp

if __name__ == "__main__":
    pass
