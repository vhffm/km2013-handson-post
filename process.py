"""
Processing Scripts for Kevin-Helmholtz Demo in Ramses.

Volker Hoffmann <volker@cheleb.net>
22 May 2013
"""

import numpy as np
from pymses import RamsesOutput
from pymses.analysis import sample_points
import matplotlib.pyplot as plt
from helpers import compute_derivatives, mkpoints
import argparse
from pymses.sources.ramses.output import *
import sys

class Viz():
    def __init__(self, iout=1):
        self.nx = 128; self.ny = 128
        self.ext = np.array([0, 1, 0, 1]) - 0.5
        self.iout = iout

    def load_ramses(self):
        print "* Loading Ramses Data..."
        # 2D data format
        RamsesOutput.amr_field_descrs_by_file = { "2D": {
            "hydro" : [ Scalar("rho", 0), Vector("vel", [1, 2]), Scalar("P", 3) ] \
        } }
        self.points = mkpoints(self.nx, self.ny)
        output = RamsesOutput(".", self.iout)
        # source = output.amr_source(["rho", "vel", "P"])
        source = output.amr_source(["rho", "vel"])
        self.info = output.info
        # @fixme - there's a bug here if we use_C_code=True (default)
        #          i.e. tree_utils.c does not work properly, but
        #          i.e. tree_utils.pyx does
        self.dset = sample_points(source, self.points, use_C_code=False)

    def compute_vorticity(self):
        print "* Computing Vorticity..."
        grid_x = self.dset.points[:,0] - 0.5
        grid_y = self.dset.points[:,1] - 0.5
        grid_x = grid_x.reshape(self.nx, self.ny)
        grid_y = grid_y.reshape(self.nx, self.ny)
        vx = self.dset["vel"][:,0].reshape(self.nx, self.ny)
        vy = self.dset["vel"][:,1].reshape(self.nx, self.ny)

        _, dvxdy = compute_derivatives(vx, grid_x, grid_y)
        dvydx, _ = compute_derivatives(vy, grid_x, grid_y)

        self.vorticity = dvydx - dvxdy

        # Y-Average
        self.vorticity_flat = np.nansum(self.vorticity, axis=0) / self.ny

    def make_plots(self, save=False, show=True):
        print "* Making Plots..."
        # Density
        fig1 = plt.figure()
        ax = fig1.add_subplot(1,1,1)
        im = ax.imshow(self.dset["rho"].reshape(self.nx, self.ny), \
                       interpolation='none', \
                       extent=self.ext)
        plt.colorbar(im)
        ax.set_title('Density')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.grid(True)
        if save: plt.savefig('Density_%05d.png' % self.iout)

        # X-Velocity
        fig2 = plt.figure()
        ax = fig2.add_subplot(1,1,1)
        im = ax.imshow(self.dset["vel"][:,0].reshape(self.nx, self.ny), \
                       interpolation='none', \
                       extent=self.ext)
        plt.colorbar(im)
        ax.set_title('X-Velocity')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.grid(True)
        if save: plt.savefig('X-Velocity_%05d.png' % self.iout)

        # Y-Velocity
        fig3 = plt.figure()
        ax = fig3.add_subplot(1,1,1)
        im = ax.imshow(self.dset["vel"][:,1].reshape(self.nx, self.ny), \
                       interpolation='none', \
                       extent=self.ext)
        plt.colorbar(im)
        ax.set_title('Y-Velocity')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.grid(True)
        if save: plt.savefig('Y-Velocity_%05d.png' % self.iout)

        # Vorticity
        fig4 = plt.figure()
        ax = fig4.add_subplot(1,1,1)
        im = ax.imshow(self.vorticity, interpolation='none', \
                       extent=self.ext)
        plt.colorbar(im)
        ax.set_title('Vorticity')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.grid(True)
        if save: plt.savefig('Vorticity_%05d.png' % self.iout)

        # Y-Averaged Vorticity
        xx = np.linspace(0, 1, self.nx) - 0.5
        fig5 = plt.figure()
        ax = fig5.add_subplot(1,1,1)
        ax.plot(xx, self.vorticity_flat, 'b-', linewidth=2)
        ax.set_title('Y-Averaged Vorticity')
        ax.set_ylabel('Vorticity')
        ax.set_xlabel('X')
        ax.set_xlim([-0.5,0.5])
        ax.grid(True)
        if save: plt.savefig('Y-Vorticity_%05d.png' % self.iout)

        if show: plt.show()

        plt.close(fig1)
        plt.close(fig2)
        plt.close(fig3)
        plt.close(fig4)
        plt.close(fig5)

if __name__ == "__main__":
    """Entry Point."""

    # # What Outputs?
    # parser = argparse.ArgumentParser()
    # parser.add_argument("imin", type=int, help='First Output')
    # parser.add_argument("imax", type=int, help='Last Output')
    # args = parser.parse_args()

    # # Sanity Checks
    # if args.imin > args.imax:
    #     print("Cannot Work With Imin > Imax. Use -h for help.")
    #     sys.exit(-1)

    # # Build Output Range
    # iouts = range(args.imin, args.imax+1)

    # What Output?
    parser = argparse.ArgumentParser()
    parser.add_argument("imin", type=int, help='First Output')
    parser.add_argument("imax", type=int, help='Last Output')
    parser.add_argument("--show", action="store_true", \
                    help='Show Figure')
    parser.add_argument("--save", action="store_true", \
                    help='Save Figure')
    args = parser.parse_args()

    # Sanity Checks
    if args.imin > args.imax:
        print("Cannot Work With Imin > Imax. Use -h for help.")
        sys.exit(-1)
    if not (args.show or args.save):
        print("Specify --save or --show. Use -h for help.")
        sys.exit(-1)
    if args.show and (args.imin != args.imax):
        print("Can only show a single output. Use -h for help.")
        sys.exit(-1)

    # Build Output Range
    iouts = range(args.imin, args.imax+1)

    # Do Stuff
    for iout in iouts:
        viz = Viz(iout)
        viz.load_ramses()
        viz.compute_vorticity()
        viz.make_plots(save=args.save, show=args.show)   
